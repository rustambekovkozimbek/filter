const python = document.querySelectorAll(".python");
const javascript = document.querySelectorAll(".javascript");
const html = document.querySelectorAll(".html");
const vue = document.querySelectorAll(".vue");

let arr = [];

let yellow = document.querySelector(".javascriptbtn");
yellow.addEventListener("click", () => {
  arr.push("javascript");
  filter();
});

let orange = document.querySelector(".htmlbtn");
orange.addEventListener("click", () => {
  arr.push("html");
  filter();
});

let purple = document.querySelector(".pyhthonbtn");
purple.addEventListener("click", () => {
  arr.push("python");
  filter();
});

let green = document.querySelector(".vuebtn");
green.addEventListener("click", () => {
  arr.push("vue");
  filter();
});

let show = document.querySelector(".show");
show.addEventListener("click", () => {
  arr = [];
  javascript.forEach((el) => {
    el.style.display = "block";
  });
  python.forEach((el) => {
    el.style.display = "block";
  });
  html.forEach((el) => {
    el.style.display = "block";
  });
  vue.forEach((el) => {
    el.style.display = "block";
  });
});

function filter() {
  if (!arr.includes("python")) {
    python.forEach((el) => {
      el.style.display = "none";
    });
  } else {
    python.forEach((el) => {
      el.style.display = "block";
    });
  }

  if (!arr.includes("javascript")) {
    javascript.forEach((el) => {
      el.style.display = "none";
    });
  } else {
    javascript.forEach((el) => {
      el.style.display = "block";
    });
  }

  if (!arr.includes("html")) {
    html.forEach((el) => {
      el.style.display = "none";
    });
  } else {
    html.forEach((el) => {
      el.style.display = "block";
    });
  }

  if (!arr.includes("vue")) {
    vue.forEach((el) => {
      el.style.display = "none";
    });
  } else {
    vue.forEach((el) => {
      el.style.display = "block";
    });
  }
}